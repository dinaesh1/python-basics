                                #### FOR LOOP ####
# 2 Tables using for loop
for i in range(2,3):
    for j in range (1,11):
        print(j,"x",i,'=',i*j)

# Printing apples for 2 times beside 1 and 2 for 10 times
for i in range (1,10):
    for j in range (1,3):
        print (j,'apple')
    print()

# Printing Weaks and days in order 
for i in range (1,5):
    print ("\bWeek: ",i)
    for j in range (1,8):
        print("Day :",j)
    print ("-----")

# '*' Pattern 
for i in range (1,6):
    print()
    for j in range(1,i+1):
        print('*', end="")