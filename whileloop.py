                                #### WHILE LOOP ####

# Printing numbers from 1 to 10
i=1
while (i<10):
    i=i+1
    print(i)

# Printing numbers from 10 to 1 
i=10
while (i>1):
    i=i-1
    print(i)

# Factorial of 5 using while loop
i=5
fact=1
while i>0:
    fact=fact*i
    i=i-1
print(fact) 